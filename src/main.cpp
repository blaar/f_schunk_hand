/*
 * hand_broker.cpp
 *
 *  Created on: Oct 2, 2014
 *      Author: Arnaud Blanchard
 *       */

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

#include <driver_svh/SVHFingerManager.h>

#include <limits.h>
#include <getopt.h>

#include "blc_core.h"
#include "blc_channel.h"
#include "blc_program.h"

#define DEFAULT_DEVICE "/dev/ttyUSB0"
#define DEFAULT_BROKER_NAME "/schunk_hand"
#define MOTORS_NB driver_svh::eSVH_DIMENSION

static void command_positions(driver_svh::SVHFingerManager *hand, float *positions)
{
  int i;
  FOR(i, MOTORS_NB) hand->setTargetPosition((driver_svh::SVHChannel)i, positions[i], 0);
}

/*
void get_positions(float *positions)
{
  double value;
  int i;

  FOR(i, MOTORS_NB)
  {
    hand.getPosition((driver_svh::SVHChannel)i, value);
    positions[i]=value;
  }
}


void get_torques(float *torques)
{
  double value;
  int i;

  FOR(i, MOTORS_NB)
  {
    hand.getCurrent((driver_svh::SVHChannel)i, value);
    torques[i]=value;
  }
}*/

int main(int argc, char**argv)
{

  blc_channel set_positions;
  driver_svh::SVHFingerManager hand;

  char const *device_name=DEFAULT_DEVICE, *broker_name;
  char tmp_name[NAME_MAX+1];

  blc_program_set_description("Broker for the five finger Schunk hand");
  blc_program_add_option(&broker_name, 'b', "broker", "string", "Name of the broker", DEFAULT_BROKER_NAME);
  blc_program_add_parameter(&device_name, "path", 0, "Address of the hand", DEFAULT_DEVICE);
  blc_program_init(&argc, &argv, NULL);

  fprintf(stderr, "device\t%s\nbroker:\t%s\n", device_name, broker_name);
  fprintf(stderr, "Initializing the hand ...\n");

  SPRINTF(tmp_name, "%s.set_positions", broker_name);
  set_positions.create_or_open(tmp_name, BLC_CHANNEL_WRITE, 'FL32', 'NDEF', 1, MOTORS_NB); //write because of  READ only does not allow yet to create itself channel

  if (!hand.connect(device_name)) EXIT_ON_ERROR("Fail to connect to the hand '%s'", device_name);
  hand.resetChannel(driver_svh::eSVH_ALL);

  fprintf(stderr, "The hand is ready on path %s.\n", broker_name);

  BLC_COMMAND_LOOP(10000){
     command_positions(&hand, set_positions.floats);
  }

  hand.disconnect();
  return 0;
}
